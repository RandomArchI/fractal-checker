from os import environ, path, listdir
from os.path import join
from sys import argv
import subprocess
import scipy
import scipy.ndimage
import numpy as np
from re import sub, findall
import glob

#Returns:
#	build_output - some string
#	build_success - True if build succeded
def build():
	arc_name = '/code/fractal.zip'
	build_output = ""
	proc_output = subprocess.run(["unzip", arc_name, "-d", "."], stdout=subprocess.PIPE, timeout=60, stderr=subprocess.STDOUT)
	if not path.isfile('CMakeLists.txt'):
		return "Incorrect archive structure\n" + str(proc_output.stdout), False
	proc_output = subprocess.run(["cmake", "-j1", "-DCMAKE_BUILD_TYPE=Release", "."], stdout=subprocess.PIPE, timeout=60, stderr=subprocess.STDOUT)
	build_output += str(proc_output.args) + '\n' + str(proc_output.stdout) + "\n\n"
	proc_output = subprocess.run(["cmake", "--build", ".", "--config", "Release", "--", "-j", "1"], stdout=subprocess.PIPE, timeout=60, stderr=subprocess.STDOUT)
	build_output += str(proc_output.args) + '\n' + str(proc_output.stdout) + "\n"
	if not path.isfile('./FractalCompression'):
		return "Build failed\n" + build_output, False
	return "", True
	
def grade(results_list):
	description = ""
	totl_cnt = 3 * len(results_list)
	good_cnt = 0
	
	tot_time = 0 #Total time
	tot_psnr = 0 #PSNR
	tot_size = 0 #Raw size
	tot_comp = 0 #Compressed size
	
	print(results_list)
	for result in results_list:
		for cur in result['result'].split('\n'):
			res = findall("=([0-9\.]*)[,.]", cur)
			if len(res) != 0:
				good_cnt += 1
				tot_time += float(res[0])
				tot_size += float(res[2])
				tot_psnr += float(res[1])
				tot_comp += float(res[3])
	description = "Total time: {}\nTotal PSNR: {}\nCompression rate: {}".format(tot_time, tot_psnr, tot_comp / tot_size)
	mark = good_cnt / totl_cnt * 100
	return '\n' + description, mark

def check_test(output_dir, gt_dir):
	src_img = scipy.ndimage.imread(gt_dir + "/img.bmp")
	desc = ""
	
	file = open(output_dir + "/time.txt", "r")
	
	for step_value in ["4", "8", "16"]:
		desc += "For step " + step_value + " "
		data = file.readline().strip().split(' ')
		if len(data) == 1:
			#Something went wrong
			desc += data[0] + ".\n"
		else:
			res_img = scipy.ndimage.imread(output_dir + "/res" + step_value + ".bmp")
			if res_img.shape[0:1] != src_img.shape[0:1]:
				desc += "WA.\n"
			else:
				if len(res_img.shape) != len(src_img.shape):
					src_img = np.expand_dims(src_img, 2)
					src_img = np.repeat(src_img, 3, 2)
				psnr = 10 * np.log10(255 ** 2 / np.mean((res_img - src_img) ** 2))
				before = path.getsize(gt_dir + "/img.bmp")
				after = path.getsize(output_dir + "/arc" + step_value + ".fr")
				#Time, quality, compression
				desc += "time={}, psnr={}, uncompressed={}, compressed={}.\n".format(data[1], psnr, before, after)
		
	file.close()
	return desc
	
def run_single_test(data_dir, output_dir):
	from time import time
	build_output, build_success = build()
	if not build_success:
		print("Build error:\n", build_output)
		raise RuntimeError(build_output)
	
	result = []
	
	for step_value in ["4", "8", "16"]:
		print("For step", step_value + ":")
		start = time()
		
		try:
			output = subprocess.run(["./FractalCompression", "-c", data_dir + "/img.bmp", output_dir + "/arc" + step_value + ".fr", "-s", step_value], timeout=300, check=True)
		except subprocess.TimeoutExpired:
			result.append({'result': 'TL1'})
			print("Time limit during compression")
			continue
		except subprocess.CalledProcessError:
			result.append({'result': 'RE1'})
			print("Runtime error during compression")
			continue
			
		#Compression finished, starting decompression
		try:
			output = subprocess.run(["./FractalCompression", "-d", output_dir + "/arc" + step_value + ".fr", output_dir + "/res" + step_value + ".bmp"], timeout=300, check=True)
		except subprocess.TimeoutExpired:
			result.append({'result': 'TL2'})
			print("Time limit during decompression")
			continue
		except subprocess.CalledProcessError:
			result.append({'result': 'RE2'})
			print("Runtime error during decompression")
			continue
			
		end = time()
		running_time = end - start
		result.append({'result': 'OK', 'time': running_time})
		
	file = open(output_dir + "/time.txt", "w")	
	for cur in result:
		file.write(cur['result'])
		if 'time' in cur:
			file.write(' ' + str(cur['time']))
		file.write('\n')
	file.close()

if __name__ == '__main__':
	if environ.get('CHECKER'):
		#Script is running in testing system, run on single input
		if len(argv) != 3:
			print('Usage: %s data_dir output_dir' % argv[0])
			exit(0)
		run_single_test(argv[1], argv[2])
		pass
	else:
		# Script is running locally, run on dir with tests
		if len(argv) != 2:
			print('Usage: %s tests_dir' % argv[0])
			exit(0)

		from glob import glob
		from time import time
		from traceback import format_exc
		from os import makedirs
		
		tests_dir = argv[1]
		
		results = []
		for input_dir in sorted(glob(join(tests_dir, '[0-9][0-9]_input'))):
			print("Started test on " + sub('_input$', '', input_dir))
			output_dir = sub('input$', 'output', input_dir)
			makedirs(output_dir, exist_ok=True)
			gt_dir = sub('input$', 'gt', input_dir)

			try:
				start = time()
				run_single_test(input_dir, output_dir)
				end = time()
				running_time = end - start
			except:
				result = 'Runtime error'
				traceback = format_exc()
			else:
				try:
					result = check_test(output_dir, gt_dir)
				except:
					result = 'Checker error'
					traceback = format_exc()

			test_num = input_dir[-8:-6]
			if result == 'Runtime error' or result == 'Checker error':
				print(test_num, result, '\n', traceback)
				results.append({'result': result})
			else:
				print(test_num, '%.2fs' % running_time, result)
				results.append({
					'time': running_time,
					'result': result})

		description, mark = grade(results)
		print('Mark:', mark, description)